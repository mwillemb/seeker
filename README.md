# Seeker

## The project

The objective of this Gitlab project is to collect information for SeekerDebugger. The code can be found in its [Github repository](https://github.com/maxwills/SeekerDebugger).

## Documentation.
Application details and examples can be found in the [Wiki](https://gitlab.inria.fr/mwillemb/seeker/-/wikis/home).

## Features request.

Use this board to request a feature. [Feature Requests Board](https://gitlab.inria.fr/mwillemb/seeker/-/boards/6179)

## Bug report.

Report bugs here.
[GitHub Issues](https://github.com/maxwills/SeekerDebugger/issues)

The debugger code can be found in its [Github repository](https://github.com/maxwills/SeekerDebugger).
